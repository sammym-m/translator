package com.sam.macharia.convertor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void convert (View v){
        EditText input = findViewById(R.id.inp);
        TextView out = findViewById(R.id.res);
        String mms = input.getText().toString();
        float mm = 0;
        double result=0;
        if(! "".equals(mms)){
           // mm = Integer.parseInt(mms);
            mm = Float.parseFloat(mms);

            result = mm/25.4;
        } else {
            result = 0;
        }
        out.setText(String.valueOf(result));

    }
}
